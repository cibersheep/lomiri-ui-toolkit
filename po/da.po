# Danish translation for lomiri-ui-toolkit
# Copyright (c) 2014 Rosetta Contributors and Canonical Ltd 2014
# This file is distributed under the same license as the lomiri-ui-toolkit package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2014.
#
msgid ""
msgstr ""
"Project-Id-Version: lomiri-ui-toolkit\n"
"Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>\n"
"POT-Creation-Date: 2015-11-05 10:05+0100\n"
"PO-Revision-Date: 2014-08-01 10:18+0000\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: Danish <da@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Launchpad-Export-Date: 2017-04-05 07:13+0000\n"
"X-Generator: Launchpad (build 18335)\n"

#: Lomiri/Components/1.2/TextInputPopover.qml:29
#: Lomiri/Components/1.3/TextInputPopover.qml:29
msgid "Select All"
msgstr "Vælg alle"

#: Lomiri/Components/1.2/TextInputPopover.qml:36
#: Lomiri/Components/1.3/TextInputPopover.qml:36
msgid "Cut"
msgstr "Klip"

#: Lomiri/Components/1.2/TextInputPopover.qml:48
#: Lomiri/Components/1.3/TextInputPopover.qml:48
msgid "Copy"
msgstr "Kopiér"

#: Lomiri/Components/1.2/TextInputPopover.qml:57
#: Lomiri/Components/1.3/TextInputPopover.qml:57
msgid "Paste"
msgstr "Indsæt"

#: Lomiri/Components/1.2/ToolbarItems.qml:143
#: Lomiri/Components/1.3/ToolbarItems.qml:143
msgid "Back"
msgstr "Tilbage"

#: Lomiri/Components/ListItems/1.2/Empty.qml:398
#: Lomiri/Components/ListItems/1.3/Empty.qml:398
msgid "Delete"
msgstr "Slet"

#: Lomiri/Components/plugin/adapters/dbuspropertywatcher_p.cpp:51
msgid "No service/path specified"
msgstr "Ingen tjeneste/sti angivet"

#: Lomiri/Components/plugin/adapters/dbuspropertywatcher_p.cpp:69
#, qt-format
msgid "Invalid bus type: %1."
msgstr "Ugyldig bustype: %1."

#. TRANSLATORS: Time based "this is happening/happened now"
#: Lomiri/Components/plugin/i18n.cpp:268
msgid "Now"
msgstr "Nu"

#: Lomiri/Components/plugin/i18n.cpp:275
#, qt-format
msgid "%1 minute ago"
msgid_plural "%1 minutes ago"
msgstr[0] "%1 minut siden"
msgstr[1] "%1 minutter siden"

#: Lomiri/Components/plugin/i18n.cpp:277
#, qt-format
msgid "%1 minute"
msgid_plural "%1 minutes"
msgstr[0] "%1 minut"
msgstr[1] "%1 minutter"

#. TRANSLATORS: Please translated these to your locale datetime format using the format specified by
#. https://qt-project.org/doc/qt-5-snapshot/qdatetime.html#fromString-2
#: Lomiri/Components/plugin/i18n.cpp:284
msgid "h:mm ap"
msgstr "HH:mm"

#. TRANSLATORS: Please translated these to your locale datetime format using the format specified by
#. https://qt-project.org/doc/qt-5-snapshot/qdatetime.html#fromString-2
#: Lomiri/Components/plugin/i18n.cpp:287
msgid "HH:mm"
msgstr "HH:mm"

#. TRANSLATORS: Please translated these to your locale datetime format using the format specified by
#. https://qt-project.org/doc/qt-5-snapshot/qdatetime.html#fromString-2
#: Lomiri/Components/plugin/i18n.cpp:293
msgid "'Yesterday 'h:mm ap"
msgstr "'I går 'HH:mm"

#. TRANSLATORS: Please translated these to your locale datetime format using the format specified by
#. https://qt-project.org/doc/qt-5-snapshot/qdatetime.html#fromString-2
#: Lomiri/Components/plugin/i18n.cpp:296
msgid "'Yesterday 'HH:mm"
msgstr "'I går 'HH:mm"

#. TRANSLATORS: Please translated these to your locale datetime format using the format specified by
#. https://qt-project.org/doc/qt-5-snapshot/qdatetime.html#fromString-2
#: Lomiri/Components/plugin/i18n.cpp:302
msgid "'Tomorrow 'h:mm ap"
msgstr "'I morgen 'HH:mm"

#. TRANSLATORS: Please translated these to your locale datetime format using the format specified by
#. https://qt-project.org/doc/qt-5-snapshot/qdatetime.html#fromString-2
#: Lomiri/Components/plugin/i18n.cpp:305
msgid "'Tomorrow 'HH:mm"
msgstr "'I morgen 'HH:mm"

#. TRANSLATORS: Please translated these to your locale datetime format using the format specified by
#. https://qt-project.org/doc/qt-5-snapshot/qdatetime.html#fromString-2
#: Lomiri/Components/plugin/i18n.cpp:312
msgid "ddd' 'h:mm ap"
msgstr "ddd' 'HH:mm"

#. TRANSLATORS: Please translated these to your locale datetime format using the format specified by
#. https://qt-project.org/doc/qt-5-snapshot/qdatetime.html#fromString-2
#: Lomiri/Components/plugin/i18n.cpp:315
msgid "ddd' 'HH:mm"
msgstr "ddd' 'HH:mm"

#. TRANSLATORS: Please translated these to your locale datetime format using the format specified by
#. https://qt-project.org/doc/qt-5-snapshot/qdatetime.html#fromString-2
#: Lomiri/Components/plugin/i18n.cpp:322
msgid "ddd d MMM' 'h:mm ap"
msgstr "ddd d. MMM 'HH:mm"

#. TRANSLATORS: Please translated these to your locale datetime format using the format specified by
#. https://qt-project.org/doc/qt-5-snapshot/qdatetime.html#fromString-2
#: Lomiri/Components/plugin/i18n.cpp:325
msgid "ddd d MMM' 'HH:mm"
msgstr "ddd d. MMM' 'HH:mm"

#: Lomiri/Components/plugin/privates/listitemdragarea.cpp:122
msgid ""
"ListView has no ViewItems.dragUpdated() signal handler implemented. No "
"dragging will be possible."
msgstr ""
"ListView har ingen signalhåndtering implementeret for "
"ViewItems.dragUpdated(). Træk-og-slip er ikke muligt."

#: Lomiri/Components/plugin/statesaverbackend_p.cpp:176
#, qt-format
msgid ""
"property \"%1\" of object %2 has type %3 and cannot be set to value \"%4\" "
"of type %5"
msgstr ""
"egenskaben \"%1\" for objektet %2 har typen %3, og kan ikke angives til "
"værdien \"%4\" med typen %5"

#: Lomiri/Components/plugin/statesaverbackend_p.cpp:185
#, qt-format
msgid "property \"%1\" does not exist or is not writable for object %2"
msgstr ""
"egenskaben \"%1\" findes ikke, eller også mangler objektet %2 skriveadgang"

#: Lomiri/Components/plugin/ucalarm.cpp:41
#: Lomiri/Components/plugin/ucalarm.cpp:643
msgid "Alarm"
msgstr "Alarm"

#: Lomiri/Components/plugin/ucalarm.cpp:635
#: Lomiri/Components/plugin/ucalarm.cpp:667
msgid "Alarm has a pending operation."
msgstr "Alarmen har en afventende handling."

#: Lomiri/Components/plugin/ucarguments.cpp:188
msgid "Usage: "
msgstr "Brug: "

#: Lomiri/Components/plugin/ucarguments.cpp:209
msgid "Options:"
msgstr "Tilvalg:"

#: Lomiri/Components/plugin/ucarguments.cpp:498
#, qt-format
msgid "%1 is expecting an additional argument: %2"
msgstr "%1 forventer et argument yderligere: %2"

#: Lomiri/Components/plugin/ucarguments.cpp:503
#, qt-format
msgid "%1 is expecting a value for argument: %2"
msgstr "%1 forventer en værdi for argumentet: %2"

#: Lomiri/Components/plugin/ucarguments.cpp:520
#, qt-format
msgid "%1 is expecting additional arguments: %2"
msgstr "%1 forventer yderligere argumenter: %2"

#: Lomiri/Components/plugin/uclistitemstyle.cpp:145
msgid "consider overriding swipeEvent() slot!"
msgstr "overvej at tilsidesætte soklen for swipeEvent()!"

#: Lomiri/Components/plugin/uclistitemstyle.cpp:165
msgid "consider overriding rebound() slot!"
msgstr "overvej at tilsidesætte soklen for rebound()!"

#: Lomiri/Components/plugin/ucmousefilters.cpp:1065
msgid "Ignoring AfterItem priority for InverseMouse filters."
msgstr "Ignorerer AfterItem-prioriteten for InverseMouse-filtre."

#: Lomiri/Components/plugin/ucserviceproperties.cpp:77
msgid "Changing connection parameters forbidden."
msgstr "Ændring af forbindelsesparametre er ikke tilladt."

#: Lomiri/Components/plugin/ucserviceproperties.cpp:160
#, qt-format
msgid ""
"Binding detected on property '%1' will be removed by the service updates."
msgstr ""
"Bindingen som er registreret for egenskaben \"%1\" vil blive fjernet af "
"tjenesteopdateringerne."

#: Lomiri/Components/plugin/ucstatesaver.cpp:46
msgid "Warning: attachee must have an ID. State will not be saved."
msgstr "Advarsel: attachéen skal have et id. Tilstanden vil ikke blive gemt."

#: Lomiri/Components/plugin/ucstatesaver.cpp:56
#, qt-format
msgid ""
"Warning: attachee's UUID is already registered, state won't be saved: %1"
msgstr ""
"Advarsel: attachéens UUID er allerede registreret, tilstanden vil ikke blive "
"gemt: %1"

#: Lomiri/Components/plugin/ucstatesaver.cpp:107
#, qt-format
msgid ""
"All the parents must have an id.\n"
"State saving disabled for %1, class %2"
msgstr ""
"Alle overelementer skal have et id.\n"
"Lagring af tilstand er slået fra for %1, klasse %2"

#: Lomiri/Components/plugin/uctheme.cpp:208
#, qt-format
msgid "Theme not found: \"%1\""
msgstr "Temaet blev ikke fundet: \"%1\""

#: Lomiri/Components/plugin/uctheme.cpp:539
msgid "Not a Palette component."
msgstr "Ikke en paletkomponent."

#: Lomiri/Components/plugin/ucviewitemsattached.cpp:462
msgid "Dragging mode requires ListView"
msgstr "Træk-og-slip-tilstanden kræver ListView"

#: Lomiri/Components/plugin/ucviewitemsattached.cpp:468
msgid ""
"Dragging is only supported when using a QAbstractItemModel, ListModel or "
"list."
msgstr ""
"Træk-og-slip understøttes kun når der bruges  QAbstractItemModel, ListModel "
"eller en liste."

#: Lomiri/Components/Popups/1.2/ComposerSheet.qml:78
#: Lomiri/Components/Popups/1.3/ComposerSheet.qml:78
msgid "Cancel"
msgstr "Annullér"

#: Lomiri/Components/Popups/1.2/ComposerSheet.qml:88
#: Lomiri/Components/Popups/1.3/ComposerSheet.qml:88
msgid "Confirm"
msgstr "Godkend"

#: Lomiri/Components/Popups/1.2/DefaultSheet.qml:85
#: Lomiri/Components/Popups/1.3/DefaultSheet.qml:85
msgid "Close"
msgstr "Luk"

#: Lomiri/Components/Popups/1.2/DefaultSheet.qml:95
#: Lomiri/Components/Popups/1.3/DefaultSheet.qml:95
msgid "Done"
msgstr "Udført"

#: Lomiri/Components/Themes/Ambiance/1.2/ProgressBarStyle.qml:51
#: Lomiri/Components/Themes/Ambiance/1.3/ProgressBarStyle.qml:50
msgid "In Progress"
msgstr "I gang"

#: Lomiri/Components/Themes/Ambiance/1.2/PullToRefreshStyle.qml:28
#: Lomiri/Components/Themes/Ambiance/1.3/PullToRefreshStyle.qml:28
msgid "Release to refresh..."
msgstr "Slip for at opdatere..."

#: Lomiri/Components/Themes/Ambiance/1.2/PullToRefreshStyle.qml:28
#: Lomiri/Components/Themes/Ambiance/1.3/PullToRefreshStyle.qml:28
msgid "Pull to refresh..."
msgstr "Træk for at opdatere..."

#~ msgid "Theme not found: "
#~ msgstr "Temaet blev ikke fundet: "
